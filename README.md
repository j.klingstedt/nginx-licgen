# nginx-licgen

Nginx Controller developer license generator

### Steps to install

1. Clone this repo
2. `cd nginx-licgen`
3. `yarn install`
4. `yarn link`

Now you can use `nginx-licgen` in any directory you choose.

| Flag | Desc                              | Default                             |
| ---- | --------------------------------- | ----------------------------------- |
| `-h` | help                              | display all possible flags          |
| `-n` | name of generated license         | `license.txt`                       |
| `-t` | license type                      | `paid`                              |
| `-p` | product code                      | `INSTANCE_MANAGER`                  |
| `-f` | feature flag                      | n/a                                 |
| `-l` | license limits                    | `420`                               |
| `-e` | expiry                            | one year from the current timestamp |
| `-x` | expire in supplied minutes        | n/a                                 |
| `-g` | grace period in days              | `2`                                 |
| `-v` | license version                   | `2`                                 |
| `-b` | base 64 encode the file data      | `false`                             |
| `-c` | copy license content to clipboard | `false`                             |

### Example

`nginx-licgen -n data-limit -l 420 -f data_per_hour_gb -c -b`

The above example will create a license file named `data-limit.txt` with a data limit of 420 GB/hour feature flag and copy the base64 encoded contents to your clipboard.
