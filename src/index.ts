import { Command, flags } from "@oclif/command";
import cli from "cli-ux";
import axios from "axios";
import { writeFile } from "fs";
import * as clipboard from "clipboardy";
import btoa from "btoa";

interface PayLoadTypes {
    licenseVersion: number;
    orderType: string;
    expiry: any;
    gracePeriodDays?: number;
    features: [
        {
            productCode?: string;
            limits?: number;
            featureFlag?: string;
        }
    ];
}

class NginxLicgen extends Command {
    static description = "describe the command here";

    static flags = {
        licenseVersion: flags.integer({
            char: "v",
            description: "license version",
        }),
        help: flags.help({ char: "h" }),
        productCode: flags.string({ char: "p" }),
        type: flags.string({ char: "t", description: "type of license" }),
        limits: flags.string({ char: "l", description: "license limits" }),
        expiry: flags.string({ char: "e", description: "expiry" }),
        expireSoon: flags.integer({ char: "x", description: "expire soon" }),
        gracePeriodDays: flags.string({
            char: "g",
            description: "grace period days",
        }),
        featureFlag: flags.string({ char: "f", description: "feature flag" }),
        name: flags.string({ char: "n", description: "name of file" }),
        copy: flags.boolean({
            char: "c",
            description: "copy file data to clipboard",
        }),
        basee64encode: flags.boolean({
            char: "b",
            description: "base 64 encode the file data",
        }),
    };

    static args = [{ name: "file" }];

    generateIsoDate() {
        const oneYear = 365 * 24 * 60 * 60 * 1000;
        const tempDate = Date.now() + oneYear;
        return new Date(tempDate);
    }

    generateSoonToExpire(minutes: number) {
        const newTimeStamp = Date.now() + minutes * 60 * 1000;
        return new Date(newTimeStamp);
    }

    formatPayload(flags: any) {
        let expiry = this.generateIsoDate();
        if (flags.expiry) {
            expiry = flags.expiry;
        } else if (flags.expireSoon) {
            expiry = this.generateSoonToExpire(parseInt(flags.expireSoon, 0));
        }
        const payload: PayLoadTypes = {
            licenseVersion: flags.licenseVersion || 2,
            orderType: flags.type || "paid",
            expiry,
            gracePeriodDays: parseInt(flags.gracePeriodDays, 0) || 2,
            features: [
                {
                    productCode: flags.productCode || "INSTANCE_MANAGER",
                    limits: 420,
                    featureFlag: "",
                },
            ],
        };

        flags.limits
            ? (payload.features[0].limits = parseInt(flags.limits, 10))
            : delete payload.features[0].limits;
        flags.featureFlag
            ? (payload.features[0].featureFlag = flags.featureFlag)
            : delete payload.features[0].featureFlag;

        return payload;
    }

    async run() {
        const { flags } = this.parse(NginxLicgen);
        const fileName = flags.name || "license";
        const payload = this.formatPayload(flags);
        console.log(payload);
        cli.action.start("Generating license file", "Fetching...");

        axios
            .post(
                "https://f5-nonprod-webdev-test.apigee.net/ee/v1/utility/license/controller",
                payload
            )
            .then((response) => {
                let licenseData = response.data;
                cli.action.stop();
                if (flags.copy) {
                    if (flags.basee64encode) {
                        licenseData = btoa(licenseData);
                    }
                    clipboard.writeSync(licenseData);
                    clipboard.readSync();
                }
                writeFile(`${fileName}.txt`, licenseData, () => {
                    console.info(`${fileName}.txt written`);
                });
            })
            .catch((error) => {
                cli.action.stop();
                const oclifHandler = require("@oclif/errors/handle");
                return oclifHandler(error);
            });
    }
}

export = NginxLicgen;
