const createSoonToExpireDate = () => {
    const newTimeStamp = Date.now() - (5 * 60 * 1000);
    console.log(new Date(newTimeStamp).toISOString());
}

createSoonToExpireDate("2022-02-10T23:07:55.845Z");
